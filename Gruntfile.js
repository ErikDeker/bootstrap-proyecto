module.exports = function(grunt) {
	require('time-grunt')(grunt);
	require('jit-grunt')(grunt, {
		useminPrepare: 'grunt-usemin',
		htmlmin: 'grunt-contrib-htmlmin'
	});
	grunt.initConfig({
		sass: {
			dist: {
				files: [{
					expand: true,
					cwd: 'css',
					src: ['*.scss'],
					dest: 'css',
					ext: '.css'
				}]
			}
		},
		
		watch: {
			files: ['css/*.scss'],
			tasks: ['css'],
		},
		
		browserSync: {
			dev: {
				bsFiles:{
					src : [
						'css/*.css',
						'*.html',
						'js/*.js'
					]
				},
				options: {
					watchTask: true,
					server: {
						baseDir: "./"
					}
				}
			}
		},
		
		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: './',
					src: ['img/*.{png,jpg,jpeg,gif}'],
					dest: 'dist/'
				}]
			}
		},
		
		copy: {
			html: {
				files: [{
					expand: true,
					dot: true,
					cwd: './',
					src: ['*.html'],
					dest: 'dist'
				}]
			},
		},
		
		clean: {
			build: {
				src: ['dist/']
			}
		},
		
		cssmin: {
			dist: {}
		},
		
		uglify: {
			dist: {}
		},
		
		filerev: {
			options: {
				encoding: 'utf8',
				algorithm: 'md5',
				length: 20
			},
			release: {
				files: [{
					src: [
						'dist/js/*.js',
						'dist/css/*.css',
					]
				}]
			}
		},
			
		concat: {
			options: {
				separator: ';'
			},
			dist: {}
		},
		
		useminPrepare: {
			foo: {
				dest: 'dist',
				src: ['index.html']
			},
			options: {
				flow: {
					steps: {
						css: ['cssmin'],
						js: ['uglify']
					},
					post: {
						css: [{
							name: 'cssmin',
							createConfig: function (context, block) {
								var generated = context.options.generated;
								generated.options = {
								  keepSpecialComments: 0,
								  rebase: false
								}
							}
						}]
					}
				}
			}
		},
		
		usemin: {
			html: ['dist/index.html', 'dist/about.html', 'dist/precios.html', 'dist/contacto.html'],
			options: {
				assetsDir: ['dist', 'dist/css', 'dist/js']
			}
		},
		
		htmlmin: {                                     // Task
			dist: {                                      // Target
				options: {                                 // Target options
					removeComments: true,
					collapseWhitespace: true
				},
				files: {                                   // Dictionary of files
					'dist/index.html': 'dist/index.html',     // 'destination': 'source'
					'dist/about.html': 'dist/about.html'
				}
			},
			dev: {                                       // Another target
			  files: {
				'dist/index.html': 'dist/index.html',
				'dist/about.html': 'dist/about.html'
			  }
			}
		}
	});

	grunt.registerTask('htmlmin', ['htmlmin']);
	grunt.registerTask('css', ['sass']);
	grunt.registerTask('default', ['browserSync','watch']);
	grunt.registerTask('img:compress', ['imagemin']);
	grunt.registerTask('build', [
		'clean',
		'copy',
		'imagemin',
		'useminPrepare',
		'concat',
		'cssmin',
		'uglify',
		'filerev',
		'usemin'
		]);
};