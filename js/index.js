$(function () {
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
});	
$('.carousel').carousel({
  interval: 2000
});
$('#contacto').on('show.bs.modal', function (e) {
	console.log('el modal se est� mostrando');
	$('#infoBtn').removeClass('btn-outline-primary');
	$('#infoBtn').addClass('btn-outline-secondary');
	$('#infoBtn').prop('disabled',true);
});
$('#contacto').on('shown.bs.modal', function (e) {
	console.log('el modal se mostr�');
});
$('#contacto').on('hide.bs.modal', function (e) {
	console.log('el modal se oculta');
});
$('#contacto').on('hidden.bs.modal', function (e) {
	console.log('el modal se ocult�');
	$('#infoBtn').removeClass('btn-outline-secondary');
	$('#infoBtn').addClass('btn-outline-primary');
	$('#infoBtn').prop('disabled',false);
});		